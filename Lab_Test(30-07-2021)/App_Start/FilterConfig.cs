﻿using System.Web;
using System.Web.Mvc;

namespace Lab_Test_30_07_2021_
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
