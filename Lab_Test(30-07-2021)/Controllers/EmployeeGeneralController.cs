﻿using Newtonsoft.Json;
using SalesSolution.Web.DataLayer;
using SalesSolution.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesSolution.Web.Controllers
{
    public class EmployeeGeneralController : Controller
    {
        private readonly EmployeeInformationDaL _informationDaL;

        public EmployeeGeneralController()
        {
            this._informationDaL = new EmployeeInformationDaL();
        }

        // GET: EmployeeGeneral
        public ActionResult EmployeeRecords()
        {
            
            if (Session["UserId"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("SignIn", "UserLogin");
            }
        }


        public ActionResult EmployeeSetup(int id = 0)
        {
            
            if (Session["UserId"] != null)
            {
                ViewBag.Id = id;
                return View();
            }
            else
            {
                return RedirectToAction("SignIn", "UserLogin");
            }

        }

        public ActionResult Save_EmployeeInformation(EmployeeInformation employee)
        {
       
            if (Session["UserId"] != null)
            {
                return Json(_informationDaL.SaveEmployeeInformation(employee, Session["UserId"].ToString()), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("SignIn", "UserLogin");
            }
        }

        public ActionResult GetDesignation()
        {
            DataTable dt = _informationDaL.GetDesignationForDDL();
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(dt);
            return Json(JSONresult, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetEmployeeInformationList()
        {
            DataTable dt = _informationDaL.GetEmployeeInformationList();
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(dt);
            return Json(JSONresult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeInfoDataForEdit(int id)
        {
            return Json(_informationDaL.GetNoticeMasterForEdit(id), JsonRequestBehavior.AllowGet);
        }

       
        public ActionResult DailyShiftSetup()
        {
            return View();
        }
    }
}