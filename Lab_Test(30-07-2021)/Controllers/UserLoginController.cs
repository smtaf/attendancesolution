﻿using SalesSolution.Web.DataLayer;
using SalesSolution.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesSolution.Web.Controllers
{
    public class UserLoginController : Controller
    {
      


        public ActionResult SignIn()
        {
            return View();
        }


        public ActionResult Registration()
        {
            return View();
        }


        public ActionResult LogOut()
        {
           Session["UserId"] ="";
           Session["UserName"] = "";
            Session["LoginName"] = "";
            Session["Email"] = "";
            Response.Redirect("SignIn");
            return View();
        }


        public JsonResult GetLoginInfo(LoginInfoDAO model)
        {


            LoginInfoDAL aLoginInfoDAL = new LoginInfoDAL();

            List<LoginInfoDAO> lst = aLoginInfoDAL.LoginInfoUSerPassDAL(model.LoginName, model.Password);

            string result = "fail";
            if (lst.Count() > 0)
            {

            
                result = "Success";

            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}