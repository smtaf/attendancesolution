﻿using Lab_Test_30_07_2021_.DataLayer;
using Lab_Test_30_07_2021_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lab_Test_30_07_2021_.Controllers
{
    public class AttendanceController : Controller
    {
        // GET: Attendance

        private readonly AttendanceDal informationDal;


       public AttendanceController()
        {
            this.informationDal = new AttendanceDal();
        }


        public ActionResult AttendanceEntry()
        {
            return View();
        }

        public ActionResult Save_Attendance(Attendance attendance)
        {
            if (Session["UserId"] != null)
            {
                 return Json(informationDal.Save_AttendanceInformation(attendance, Convert.ToInt32(Session["UserId"].ToString())), JsonRequestBehavior.AllowGet);           
            }
            else
            {
                return RedirectToAction("SignIn", "UserLogin");
            }
        }
    }
}