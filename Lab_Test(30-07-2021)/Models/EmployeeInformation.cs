﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesSolution.Web.Models
{
    public class EmployeeInformation
    {

        public int EmpId { get; set; }

        public string ID { get; set; }

        public string Name { get; set; }

        public int? Age { get; set; }

        public string Designation { get; set; }

        public int? EntryBy { get; set; }

        public DateTime? EntryDate { get; set; }

        public int? UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

    }
}