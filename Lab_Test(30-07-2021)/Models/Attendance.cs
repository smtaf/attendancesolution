﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lab_Test_30_07_2021_.Models
{
    public class Attendance
    {
        public int AttId { get; set; }

        public int? EmpID { get; set; }

        public DateTime? CheckTime { get; set; }

        public bool? IsCheckIn { get; set; }

        public bool? IsCheckOut { get; set; }

        public string ImageURL { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Note { get; set; }

        public int number { get; set; }
    }
}