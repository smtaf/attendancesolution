﻿using SalesSolution.Web.DataManager;
using SalesSolution.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SalesSolution.Web.DataLayer
{
    public class EmployeeInformationDaL
    {
        private DataAccessManager accessManager = new DataAccessManager();


        public ResultInfo SaveEmployeeInformation(EmployeeInformation employee, string sessionUser)
        {
            int pk = 0;
            ResultInfo aInformation = new ResultInfo();
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                DateTime entryDtae = DateTime.Now;
                gSqlParameterList.Add(new SqlParameter("@id", employee.EmpId));
                gSqlParameterList.Add(new SqlParameter("@Name", employee.Name));
                gSqlParameterList.Add(new SqlParameter("@Age", employee.Age));
                gSqlParameterList.Add(new SqlParameter("@Designation", employee.Designation));

                if (employee.EmpId > 0)
                {
                   
                        gSqlParameterList.Add(new SqlParameter("@UpdateBy", sessionUser));
                        aInformation.isSuccess = accessManager.UpdateData("sp_Update_EmployeeInformation", gSqlParameterList);           
                   
                }
                else
                {
                    gSqlParameterList.Add(new SqlParameter("@EntryBy", sessionUser));
                    pk = accessManager.SaveDataReturnPrimaryKey("sp_Save_EmployeeInformation", gSqlParameterList);
                    if (pk > 0)
                    {
                        aInformation.isSuccess = true;
                    }
                }
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                aInformation.isSuccess = false;
                aInformation.ErrorMessage = exception.Message;

                throw exception;
            }
            finally
            {

                accessManager.SqlConnectionClose();
            }

            return aInformation;
        }

   

        public DataTable GetEmployeeInformationList()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@Parameter", ""));
                DataTable dt = accessManager.GetDataTable("sp_GET_EmployeeInfo",aSqlParameterlist);
                return dt;
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }



        public DataTable GetDesignationForDDL()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
                DataTable dt = accessManager.GetDataTable("sp_GET_DesignationForDDL");
                return dt;
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }



 


        public EmployeeInformation GetNoticeMasterForEdit(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                EmployeeInformation master = new EmployeeInformation();
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@id", id));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_EmployeeInformation_ById", aSqlParameters);
                while (dr.Read())
                {
                    master.EmpId = (int)dr["EmpId"];
                    master.Name = dr["Name"].ToString();
                    master.Age = (int)dr["Age"];
                    master.Designation = dr["Designation"].ToString();

                }
                return master;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


    }
}