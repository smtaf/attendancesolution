﻿using SalesSolution.Web.DataManager;
using SalesSolution.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SalesSolution.Web.DataLayer
{
    public class LoginInfoDAL
    {
        private DataAccessManager accessManager = new DataAccessManager();
        public List<LoginInfoDAO> LoginInfoUSerPassDAL(string userid, string password)
        {
            try
            {
                List<LoginInfoDAO> aList = new List<LoginInfoDAO>();

                accessManager.SqlConnectionOpen(DataBase.SalesDB);
                List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@loginName", userid));
                aSqlParameterlist.Add(new SqlParameter("@password", password));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GET_UserInfoAll", aSqlParameterlist);


                LoginInfoDAO aDao;

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        aDao = new LoginInfoDAO();
                        aDao.UserId = (int)dr["UserId"];

                        aDao.UserName = dr["UserName"].ToString();
             
                 
               

                   HttpContext.Current.Session["UserId"] = aDao.UserId;
                        HttpContext.Current.Session["UserName"] = aDao.UserName;
               
                        //HttpContext.Current.Session["Email"] = aDao.Email;


                        aList.Add(aDao);
                    }
                }


                return aList;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {

                accessManager.SqlConnectionClose();
            }
        }
    }
}