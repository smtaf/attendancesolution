﻿using Lab_Test_30_07_2021_.Models;
using SalesSolution.Web.DataManager;
using SalesSolution.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using static System.Net.WebRequestMethods;

namespace Lab_Test_30_07_2021_.DataLayer
{
    public class AttendanceDal
    {

        private DataAccessManager accessManager = new DataAccessManager();


        public ResultInfo Save_AttendanceInformation(Attendance attendance, int sessionUser)
        {
            int pk = 0;
            ResultInfo aInformation = new ResultInfo();
            try
            {
                accessManager.SqlConnectionOpen(DataBase.SalesDB);

                if (attendance.ImageURL != null)
                {

             
                    //var filename = "Image.jpg";
                    //string base64 = attendance.ImageURL.Split(',')[1];

                    //byte[] bytes = Convert.FromBase64String(base64);

                    //Image image;
                    //using (MemoryStream ms = new MemoryStream(bytes))
                    //{
                    //    image = Image.FromStream(ms);

                    //   var p = image.ToString();
                    //}

                    //string path = @"D:\Image\";
                    //if (image != null)
                    //{
                    //    if (!Directory.Exists(path))
                    //    {
                    //        Directory.CreateDirectory(path);
                    //    }
                    //    image.Save(path+ filename);
                    //}
                    //string fullpath = path + Path.GetFileName(filename);

                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
                    gSqlParameterList.Add(new SqlParameter("@id", attendance.AttId));
                    gSqlParameterList.Add(new SqlParameter("@EmpID", attendance.EmpID));
                    gSqlParameterList.Add(new SqlParameter("@IsCheckIn", attendance.IsCheckIn));
                    gSqlParameterList.Add(new SqlParameter("@IsCheckOut", attendance.IsCheckOut));
                    gSqlParameterList.Add(new SqlParameter("@Note", attendance.Note));
                    gSqlParameterList.Add(new SqlParameter("@ImageURL", attendance.ImageURL));
                    aInformation.isSuccess = accessManager.SaveData("sp_Save_EmpAttendance", gSqlParameterList);
                }
            }
            catch (Exception exception)
            {
               // accessManager.SqlConnectionClose(true);
                aInformation.isSuccess = false;
                aInformation.ErrorMessage = exception.Message;

                
                
                
                throw exception;
            }
            finally
            {

                accessManager.SqlConnectionClose();




            }

            return aInformation;
        }

    }
}